package com.b208.postApp.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -5814071057647969314L;

    private final String jwttoken;

    public JwtResponse(String jwtToken){
        this.jwttoken = jwtToken;
    }

    public String getToken(){
        return this.jwttoken;
    }
}
