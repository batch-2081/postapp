package com.b208.postApp.services;

import com.b208.postApp.models.User;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserServices{

    @Autowired
    private UserRepository userRepository;

    //Academic Reading List
    public boolean createUser(User user) {
        if(user.getPassword().length() < 8){
            return true;
        } else {
            userRepository.save(user);
            return false;
        }
    }
    //Lesson
    public Optional<User> findByUsername(String username){
        //.ofNullable allows us to return null
        return Optional.ofNullable(userRepository.findByUsername(username));

    }

    public Iterable<User> getUser(){
        return userRepository.findAll();
    }

    public void updateUser(Long id, User user){
        User userUpdate = userRepository.findById(id).get();
        userUpdate.setUsername(user.getUsername());
        userUpdate.setPassword(user.getPassword());
        userRepository.save(userUpdate);
    }

    public void deleteUser(Long id){
        userRepository.deleteById(id);
    }

    //Academic Reading List
//    public Optional<User> searchUser(Long id){
//        return userRepository.findById(id);
//    }
//
//    public boolean checkUser(Long id) {
//        if (userRepository.existsById(id) == true){
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    public long countUser() {
//        return userRepository.count();
//    }

}
