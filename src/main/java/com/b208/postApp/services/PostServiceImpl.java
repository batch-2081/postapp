package com.b208.postApp.services;

import com.b208.postApp.config.JwtToken;
import com.b208.postApp.models.Post;
import com.b208.postApp.models.User;
import com.b208.postApp.repositories.PostRepository;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//@Services is added so that springboot would recognize the business logic implemented in this layer
@Service
public class PostServiceImpl implements PostService {
    //@Autowired creates an instance of the postRepository that is persistent within our springboot app
    //so we can use the pre-defined methods for database manipulation for our table
    @Autowired
    private PostRepository postRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    private UserRepository userRepository;

    public boolean createPost(String stringToken, Post post) {
        //when createPost is used from our services, we will be able to pass a Post class object and then, using the .save() method of our postRepository we will be able to insert a new row into our posts table

        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        //Check if you can get the user from the table
        //System.out.println(author.getUsername());
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        //Academic Reading List
        if (post.getTitle().isEmpty() || post.getContent().isEmpty()) {
            return true;
        } else {
            //Get the username from the token using jwtToken getUsernameFromToken method
            //Then, pass the username:
            //Get the user's details from our table, using our userRepository findByUsername method
            postRepository.save(newPost);
            return false;
        }
    }

    public Iterable<Post> getPosts() {
        //returns all records from our table
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(String stringToken, Long id, Post post) {
        //post parameter here is actually the request object passed from the controller
        //because post parameter is actually the request body converted as a post class object, it has the methods from a post class object
        //therefore, getTitle() will actually get the value of the title property from the request body.
//        System.out.println("This is the id passed as path variable:");
//        System.out.println(id);
        //findById() retrieves a record that matches the passed id
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        System.out.println(authenticatedUser.equals(postAuthor));

        if (authenticatedUser.equals(postAuthor)) {
            //After getting the user of the post, get the username of the User
//        System.out.println("This is the found data using the id");
//        System.out.println(postForUpdating.getTitle());
//
//        System.out.println("This is the request body passed from the controller");
//        System.out.println(post.getTitle());

            //The found post content will be updated with the title of the request body as post
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post Updated Successfully", HttpStatus.OK);
            //The found post content will be updated with the content of the request body as post
            //save the updates to our data
        } else {
            return new ResponseEntity<>("You are not authorized to update this post", HttpStatus.UNAUTHORIZED);
        }
    }
        public ResponseEntity deletePost(String stringToken, Long id){
            Post postForDeleting = postRepository.findById(id).get();
            String postAuthor = postForDeleting.getUser().getUsername();
            String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

            if (authenticatedUser.equals(postAuthor)) {
                postRepository.deleteById(id);
                return new ResponseEntity<>("Post Deleted Successfully", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("You are not authorized to delete this post", HttpStatus.UNAUTHORIZED);
            }
        }

        public Object getMyPosts(String stringToken){

            String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
            User userToRetrieve = userRepository.findByUsername(authenticatedUser);

            if (authenticatedUser.equals(userToRetrieve.getUsername())){
                return userToRetrieve.getPosts().iterator();
            } else {
                return "You are not authorized to retrieve this user's posts";
            }
        }
}
