package com.b208.postApp.services;

import com.b208.postApp.models.User;

import java.util.Optional;

public interface UserServices {

    //Create User
    boolean createUser(User user);

    Iterable<User> getUser();

    void updateUser(Long id, User user);

    void deleteUser(Long id);

    // Academic Reading List
//    Optional<User> searchUser(Long id);
//
//    boolean checkUser(Long id);
//
//    long countUser();

    //Lesson
    //Optional allows us to return null or another data
    Optional<User> findByUsername(String username);
}
